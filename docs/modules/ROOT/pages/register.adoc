= Register the Collector Extension
:navtitle: Register

Once the Antora Collector extension is installed, you need to xref:antora:extend:register-extension.adoc[register the extension] with Antora.
The extension can be registered in your Antora playbook or using the CLI.

[#playbook]
== Register in the playbook

To register the extension in your Antora playbook, add an entry to the playbook file ([.path]_antora-playbook.yml_) that specifies the name of the package under the `antora.extensions` key.
Open your Antora playbook file and add the extension as follows:

.antora-playbook.yml
[,yaml]
----
antora:
  extensions:
  - '@antora/collector-extension'
# ...
----

NOTE: The quotes are required around the package name because `@` is a special character in YAML.

[#cli]
== Register from the CLI

Alternately, you can register the PDF extension at your discretion using the `--extension` CLI option of the `antora` command:

 $ antora --extension @antora/collector-extension antora-playbook.yml