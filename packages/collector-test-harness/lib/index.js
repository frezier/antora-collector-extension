/* eslint-env mocha */
'use strict'

process.env.NODE_ENV = 'test'

const chai = require('chai')
chai.use(require('chai-fs'))
// dirty-chai must be loaded after the other plugins
// see https://github.com/prodatakey/dirty-chai#plugin-assertions
chai.use(require('dirty-chai'))
const expect = chai.expect

const { configureLogger } = require('@antora/logger')
const { promises: fsp } = require('fs')
const { Git: GitServer } = require('node-git-server')
const { once } = require('events')
const yaml = require('js-yaml')

beforeEach(() => configureLogger({ level: 'silent' }))

async function captureStdStream (stream, fn) {
  const streamWrite = stream.write
  try {
    const data = []
    stream.write = (buffer) => data.push(buffer)
    await fn()
    return data.join('')
  } finally {
    stream.write = streamWrite
  }
}

const captureStderr = captureStdStream.bind(null, process.stderr)

const captureStdout = captureStdStream.bind(null, process.stdout)

function closeServer (server) {
  return once(server.close() || server, 'close')
}

function heredoc (literals, ...values) {
  const str =
    literals.length > 1
      ? values.reduce((accum, value, idx) => accum + value + literals[idx + 1], literals[0])
      : literals[0]
  const lines = str.trimRight().split(/^/m)
  if (lines.length < 2) return str
  if (lines[0] === '\n') lines.shift()
  const indentRx = /^ +/
  const indentSize = Math.min(...lines.filter((l) => l.startsWith(' ')).map((l) => l.match(indentRx)[0].length))
  return (indentSize ? lines.map((l) => (l.startsWith(' ') ? l.substr(indentSize) : l)) : lines).join('')
}

async function updateYamlFile (filepath, data) {
  const parsed = yaml.load(await fsp.readFile(filepath), { schema: yaml.CORE_SCHEMA })
  Object.assign(parsed, data)
  await fsp.writeFile(filepath, yaml.dump(parsed, { noArrayIndent: true }), 'utf8')
}

function startGitServer (dir) {
  return new Promise((resolve, reject) => {
    const gitServer = new GitServer(dir, { autoCreate: false })
    gitServer.listen(0, { type: 'http' }, function (err) {
      err ? reject(err) : resolve([gitServer, this.address().port])
    })
  })
}

function trapAsyncError (fn) {
  return fn().then(
    (retVal) => () => retVal,
    (err) => () => {
      throw err
    }
  )
}

module.exports = {
  captureStderr,
  captureStdout,
  closeServer,
  expect,
  heredoc,
  updateYamlFile,
  startGitServer,
  trapAsyncError,
  windows: process.platform === 'win32',
}
